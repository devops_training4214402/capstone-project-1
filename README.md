In this lecture we are working on the "jenkins-jobs" branch of the java-maven-app project, which can be found here: [https://gitlab.com/twn-devops-bootcamp/latest/11-eks/java-maven-app/-/tree/jenkins-jobs](https://gitlab.com/twn-devops-bootcamp/latest/11-eks/java-maven-app/-/tree/jenkins-jobs?ref_type=heads)

The starting code can be found here: [https://gitlab.com/twn-devops-bootcamp/latest/11-eks/java-maven-app/-/tree/starting-code](https://gitlab.com/twn-devops-bootcamp/latest/11-eks/java-maven-app/-/tree/starting-code)

# Ricardo's Notes

ECR 
408391530665.dkr.ecr.us-east-1.amazonaws.com/java-maven-app

Create credentials in Jenkins to access the ECR

```awscli
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 408391530665.dkr.ecr.us-east-1.amazonaws.com
```

![[Pasted image 20240124195535.png]]

At moment to install aws-iam in the cluster use the linux version not the darwin version 

```
curl -Lo aws-iam-authenticator https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v0.6.14/aws-iam-authenticator_0.6.14_linux_amd64
chmod +x ./aws-iam-authenticator
mv ./aws-iam-authenticator /usr/local/bin/
aws-iam-authenticator help
```

EKS Endpoint
```
https://F4AC1E0977C30EBA677AE47882E7EDB8.gr7.us-east-2.eks.amazonaws.com
```


config file created inside the jenkins docker

![[Pasted image 20240131145306.png]]

Install envsubst inside of the Jenkins container

`apt-get install gettext-base`

To fix the issue with kubectl api credential error
```bash
# Download AWS CLI v2
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"

# Un-compress the archive
unzip awscliv2.zip

# Run the installer
sudo ./aws/install
```


Gitlab failed due to some special characters in the password

![[Pasted image 20240201003540.png]]
![[Pasted image 20240201003605.png]]









